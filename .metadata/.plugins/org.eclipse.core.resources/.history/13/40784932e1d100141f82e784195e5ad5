package com.tgauch.maze;

import java.awt.Point;
import java.util.ArrayList;
import java.util.PriorityQueue;

import com.tgauch.main.Main;

/**
 *
 * @author gaucht
 */
public class PathFinding {
    private ArrayList<Tile> closedList;
    private PriorityQueue<Tile> tileToCheck;
    private ArrayList<Tile> path;
    private boolean processing = false;
    private boolean hasPath = false;
    private Maze maze;
    private long sleepTime = 0;
    private int vistNumbers;
    
    public PathFinding(Maze maze)
    {
        this.maze = maze;
        this.init();
    }
    private void init()
    {
        this.closedList = new ArrayList<Tile>();
        this.tileToCheck  = new PriorityQueue<Tile>();
        this.path = new ArrayList<Tile>();
    }
    public boolean findPath()
    {
        if(!processing && !hasPath)
        {
        	this.vistNumbers = 0;
            processing = true;
            path.clear();
            closedList.clear();
            tileToCheck.clear();
            Tile start = maze.getStart();
            Tile end = this.maze.getEnd();
            if(start != null && end != null && !start.equals(end))
            {
                //Step 1 Process Start Tile
                tileToCheck.add(start);
                boolean done = false;
                //Find all Nodes to get to location
                while(!done)
                {
                    done = processOpenList(end);
                    if(done == false)
                    {
                    	hasPath = false;
                    	processing = false;
                    	return false;
                    }
                }
                //Back track to find fastest path
                done = false;
                Tile parent = end.getPrev();
                path.add(end);
                path.add(parent);
                while(!done)
                {
                    if(parent.getPrev() != null)
                    {
                        parent = parent.getPrev();
                        path.add(parent);
                    }
                    else
                    {
                        done = true;
                    }
                }
            }
            else
            {
            }
            processing = false;
            hasPath = true;
            return true;
        }
        else
        {
            //print("Didnt get new path");
           // print("Processing: "+processing);
           // print("Has Path: "+hasPath);
        	return false;
        }
    }
    public Tile getNextPoint()
    {
       if(hasNextPoint())
       {
            Tile n = path.get(path.size()-1);
            return n;
       }
       else
       {
           return null;
       }
    }
    public void goToNextPoint()
    {
        if(hasNextPoint())
        {
            path.remove(path.size()-1);
        }
    }
    public boolean hasNextPoint()
    {
        if(path.size() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private boolean processOpenList(Tile goal)
    {
    	this.vistNumbers = 0;
        while(!tileToCheck.isEmpty())
        {
            Tile n = tileToCheck.remove();
            
            if(n.north != null && n.north.state != 0)
            {
            	if(this.processTile(n.north, n, goal))
            	{
            		return true;
            	}
            }
            if(n.south != null && n.south.state != 0)
            {
            	if(this.processTile(n.south, n, goal))
            	{
            		return true;
            	}
            }
            if(n.east != null && n.east.state != 0)
            {
            	if(this.processTile(n.east, n, goal))
            	{
            		return true;
            	}
            }
            if(n.west != null && n.west.state != 0)
            {
            	if(this.processTile(n.west, n, goal))
            	{
            		return true;
            	}
            }
            try {
				Thread.sleep(this.sleepTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
           // this.drawNavTile(n.getLocation(), ColorRGBA.Red);
            if(n.state != 3 && n.state != 2)
            {
            	n.state = 5;
            }
            n.vistNumber = this.vistNumbers;
            this.vistNumbers++;
            this.closedList.add(n);
        }
        return false; //did not find end tile
    }
    private boolean processTile(Tile n, Tile parent, Tile goal)
    {

        if(n != null)
        {    	
            if(n.equals(goal))
            {      
                n.setPrev(parent);
                return true;
            }
            else
            {
               n.setPrev(parent);
               n.setG(parent.getG()+10);
               n.setH((int)this.findH(n.getLocation(), goal.getLocation()));
               if(this.checkClosedList(n) && this.checkOpenList(n))
               {
            	   tileToCheck.add(n);
               }
            }
        }
        return false;
        
    }
    private boolean checkClosedList(Tile m)
    {
        for(Tile n : closedList)
        {
            if(n.equals(m))
            {
                return false;
            }
        }
        return true;
    }
    private boolean checkOpenList(Tile n)
    {
        if(tileToCheck.contains(n))
        {
        	Object[] m = tileToCheck.toArray();
        	for(Object p : m)
        	{
        		if(((Tile)p).equals(n) && n.getF() > ((Tile) p).getF())
        		{
        			return false;
        		}
        	}
        }
        return true;
    }
    private double findH(Point a, Point b)
    {
        return (10*(Math.abs(a.x-b.x) + Math.abs(a.y-b.y)));
    }
    public void showPath()
    {
        for(Tile n : path)
        {
        	if(n.state != 2 && n.state != 3)
        	{
        		n.state = 4;
        	}
        }
    }
    public void setHasPath(boolean hasPath)
    {
    	this.hasPath = hasPath;
    }
    public void setMaze(Maze maze)
    {
    	this.maze = maze;
    }
}

