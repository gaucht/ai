package com.mapping.main;

import java.util.ArrayList;
import java.util.Random;

public class RANSAC extends Thread{
	
	public ArrayList<Model> bestModels;
	public ArrayList<DataPoint> bestInliers;
	public double maxEvaluations = 100;
	public double maxSamplings = 50;
	public double probability = .95;
	public static int minConsensus = 20;
	public static int s = 10;
	public static double t = .05;
	public boolean debug = false;
	public Model m;
	private ArrayList<ArrayList<DataPoint>> usedSets = new ArrayList<ArrayList<DataPoint>>();
	
	
	public RANSAC(Model m)
	{
		this.m = m;
		bestModels = new ArrayList<Model>();
		bestInliers = new ArrayList<DataPoint>();
	}
	
	public void run()
	{
		while(Map.currentPoints < minConsensus)
		{
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		ArrayList<DataPoint> map = new ArrayList<DataPoint>();
		DataPoint[] points = Map.points.clone();
		while(true)
		{
			//bestModels.clear();
			Random r = new Random();
			r.setSeed(System.currentTimeMillis());
			int maxInliers = 0;
			this.print("Init");
			int count = 0;
			double N = maxEvaluations;
			Model bestModel = null;
			map.clear();
			for(int i = 0; i < points.length; i++)
			{
				if(points[i] != null && !points[i].in_ransac)
				{
					map.add(points[i]);
				}
			}
			if(map.size() < minConsensus)
			{
				points = Map.points.clone();
				continue;
			}
			while(count < N && count < maxEvaluations)
			{
				this.print("LOOP: " + count + " N: " + N + " MAX: " + maxEvaluations + " POINT SIZE: "+map.size());
				ArrayList<DataPoint> sample = new ArrayList<DataPoint>();
				Model model;
				int samplings = 0;
				if(this.m.getClass().getName().equals(LineModel.class.getName()))
				{
					model = new LineModel();
				}
				else
				{
					System.err.println("Model "+m.getClass().getName()+" is not implemented");
					break;
				}
				
				while(samplings < maxSamplings)
				{
					this.print("Processing Sample");
					for(int i = 0; i < s; i++)
					{
						int index = r.nextInt(map.size()-1)+1;
						sample.add(map.get(index));
					}
					
					if(!degenerate(sample))
					{
						this.usedSets.add(sample);
						model.fit(sample);
						break;
					}
					
					samplings++;
				}
				this.print("Finding Inliers");
				ArrayList<DataPoint> inliers = new ArrayList<DataPoint>();
				for(int i = 0; i < map.size(); i++)
				{
					if(model.getDistance(map.get(i).x,map.get(i).y) <= t)
					{
						inliers.add(map.get(i));
					}
				}
				
				if(inliers.size() > maxInliers && inliers.size() >= minConsensus)
				{
					this.print("More Inliers: " + inliers.size());
					maxInliers = inliers.size();
					//bestModels.clear();
					model.setInliers(inliers);
					bestModel = model;
					bestModel.fit(bestModel.getInliers());
					Landmark landmark = Landmark.findOrCreateLandmark((LineModel)bestModel);
					landmark.found();
					
					double pInlier = (double)inliers.size()/ (double)map.size();
					double pNoOutliers = 1.0 - Math.pow(pInlier, s);
					
					N = Math.log(1-probability) / Math.log(pNoOutliers);
				}
				else
				{
					this.print("No new inliers");
				}
				
				count++;
				
			}
			if(bestModel != null)
			{
				for(int i = 0; i < bestModel.getInliers().size(); i++)
				{
					DataPoint d = bestModel.getInliers().get(i);
					this.print("Removing Point: "+d.toString());
					points[d.angle] = null;
				}
				this.bestModels.add(bestModel);
			}
			this.print("Ransac done");
		}
	}
	
	public boolean degenerate(ArrayList<DataPoint> sample)
	{
		this.print("Checking Degenerate");
		if(this.usedSets.size() == 0)
		{
			this.print("FALSE");
			return false;
		}
		for(int i = 0; i < this.usedSets.size(); i++)
		{
			ArrayList<DataPoint> currentSet = this.usedSets.get(i);
			for(int j = 0; j < sample.size(); j++)
			{
				boolean found = false;
				for(int k = 0; k < currentSet.size(); k++)
				{
					if(currentSet.get(k).equals(sample.get(j)))
					{
						found = true;
						break;
					}
				}
				if(!found)
				{
					this.print("FALSE");
					return false;
				} 
			}
		}
		this.print("TRUE");
		return true;
	}
	
	public void print(String message)
	{
		if(this.debug)
		{
			System.out.println(message);
		}
	}
}