package com.mapping.main;

import java.util.ArrayList;
import java.util.Random;

public class RANSAC extends Thread{
	
	public ArrayList<Model> bestModels;
	public ArrayList<DataPoint> bestInliers;
	public double maxEvaluations = 1000;
	public double maxSamplings = 50;
	public double probability = .95;
	public static int minConsensus = 30;
	public static int minSameLineDistance = 20;
	public static int s = 5;
	public static double t = 5;
	public boolean debug = true;
	public static ArrayList<DataPoint> map;
	public Model m;
	private ArrayList<ArrayList<DataPoint>> usedSets = new ArrayList<ArrayList<DataPoint>>();
	
	
	public RANSAC(Model m)
	{
		this.m = m;
		bestModels = new ArrayList<Model>();
		bestInliers = new ArrayList<DataPoint>();
	}
	
	public void run()
	{
		while(Map.currentPoints < minConsensus)
		{
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		map = new ArrayList<DataPoint>();
		DataPoint[] points = this.getPoints();
		int lastPointSize = 0;
		Random r = new Random();
		r.setSeed(System.currentTimeMillis());
		while(true)
		{
			//bestModels.clear();
			int maxInliers = 0;
			this.print("Init");
			int count = 0;
			double N = maxEvaluations;
			Model bestModel = null;
			map.clear();
			for(int i = 0; i < points.length; i++)
			{
				if(points[i] != null && !points[i].in_ransac)
				{
					map.add(points[i]);
				}
			}
			if(map.size() <= minConsensus || map.size() == lastPointSize)
			{
				print("Gathering new points: " + map.size() + " " + lastPointSize);
				lastPointSize = 0;
				points = this.getPoints();
				continue;
			}
			else
			{
				lastPointSize = map.size();
			}
			while(count < N && count < maxEvaluations)
			{
				this.print("LOOP: " + count + " N: " + N + " MAX: " + maxEvaluations + " POINT SIZE: "+map.size());
				ArrayList<DataPoint> sample = new ArrayList<DataPoint>();
				Model model;
				int samplings = 0;
				if(this.m.getClass().getName().equals(LineModel.class.getName()))
				{
					model = new LineModel();
				}
				else
				{
					System.err.println("Model "+m.getClass().getName()+" is not implemented");
					break;
				}
				
				while(samplings < maxSamplings)
				{
					this.print("Processing Sample");
					for(int i = 0; i < s; i++)
					{
						int index = r.nextInt(map.size()-1)+1;
						sample.add(map.get(index));
					}
					
					if(!degenerate(sample))
					{
						this.usedSets.add(sample);
						model.fit(sample);
						break;
					}
					
					samplings++;
				}
				this.print("Finding Inliers");
				ArrayList<DataPoint> inliers = new ArrayList<DataPoint>();
				for(int i = 0; i < map.size(); i++)
				{
					if(model.getDistance(map.get(i).x,map.get(i).y) <= t)
					{
						inliers.add(map.get(i));
					}
				}
				print("INLIERS: "+inliers.size());
				if(inliers.size() > maxInliers && inliers.size() >= minConsensus)
				{
					this.print("More Inliers: " + inliers.size());
					maxInliers = inliers.size();
					//bestModels.clear();
					model.setInliers(inliers);
					bestModel = model;
					bestModel.fit(bestModel.getInliers());
					Landmark landmark = Landmark.findOrCreateLandmark((LineModel)bestModel);
					landmark.found();
					
					double pInlier = (double)inliers.size()/ (double)map.size();
					double pNoOutliers = 1.0 - Math.pow(pInlier, s);
					
					N = Math.log(1-probability) / Math.log(pNoOutliers);
				}
				else
				{
					this.print("No new inliers");
				}
				
				count++;
				
			}
			if(bestModel != null)
			{
				for(int i = 0; i < bestModel.getInliers().size(); i++)
				{
					DataPoint d = bestModel.getInliers().get(i);
					this.print("Removing Point: "+d.toString());
					points[d.angle] = null;
				}
				//check distance between this and other models we already have and see if it is the same model
				boolean found = false;
				for(Model m : this.bestModels)
				{
					LineModel l = (LineModel)m;
					LineModel l2 = (LineModel)bestModel;
					if((getDistanceBetweenPoints(0, l.getPointOnLine(0), 0, l2.getPointOnLine(0)) + getDistanceBetweenPoints(100, l.getPointOnLine(100), 100, l2.getPointOnLine(100)))/2 <= RANSAC.minSameLineDistance)
					{
						//these are the same line
						//we found this line more than once
						//remove last created landmark
						print("===========THESE ARE THE SAME LINES=======");
						Landmark landmark;
						Landmark landmark2;
						if(getDistanceBetweenPoints(l.maxX, l.maxY, l.minX, l.minY) > getDistanceBetweenPoints(l2.maxX, l2.maxY, l2.minX, l2.minY))
						{
							landmark = Landmark.findOrCreateLandmark(l);
							landmark2 = Landmark.findOrCreateLandmark(l2);
						}
						else
						{
							landmark = Landmark.findOrCreateLandmark(l2);
							landmark2 = Landmark.findOrCreateLandmark(l);
						}
						landmark.found();
						Landmark.removeLandMark(landmark2);								
						found = true;
						break;
					}
				}
				if(!found)
				{
					this.bestModels.add(bestModel);
				}
				print("POINTS: ");
				for(int i = 0; i < points.length; i++)
				{
					System.out.print(" "+points[i]);
				}
			}
			this.print("Ransac done");
		}
	}
	
	public boolean degenerate(ArrayList<DataPoint> sample)
	{
		this.print("Checking Degenerate");
		if(this.usedSets.size() == 0)
		{
			this.print("FALSE");
			return false;
		}
		for(int i = 0; i < this.usedSets.size(); i++)
		{
			ArrayList<DataPoint> currentSet = this.usedSets.get(i);
			for(int j = 0; j < sample.size(); j++)
			{
				boolean found = false;
				for(int k = 0; k < currentSet.size(); k++)
				{
					if(currentSet.get(k).equals(sample.get(j)))
					{
						found = true;
						break;
					}
				}
				if(!found)
				{
					this.print("FALSE");
					return false;
				} 
			}
		}
		this.print("TRUE");
		return true;
	}
	
	public void print(String message)
	{
		if(this.debug)
		{
			System.out.println(message);
		}
	}
	public DataPoint[] getPoints()
	{
		print("GET POINTS CALLED");
		DataPoint[] d = new DataPoint[181];
		for(int i = 0; i < Map.points.length; i++)
		{
			if(Map.points[i] != null)
			{
				DataPoint newD = new DataPoint(Map.points[i].x,Map.points[i].y,Map.points[i].angle);
				d[i] = newD;
			}
			else
			{
				d[i] = null;
			}
		}
		return d;
	}
	
	public double getDistanceBetweenPoints(int x1,int y1,int x2,int y2)
	{
		return Math.sqrt(Math.pow(x1-x2, 2)+Math.pow(y1-y2, 2));
	}
}