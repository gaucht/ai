package com.tgauch.main;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

public class LogPanel extends JPanel implements ActionListener{
	
	private JButton clear;
	private JLabel title, log;
	private String logInit = "<html>", logList = "", logEnd = "</html>";
	private JScrollPane logScroll;
	private int scrollPlace = 0;
	
	public LogPanel()
	{
		this.init();
	}

	private void init()
	{
		this.log = new JLabel(this.logInit+this.logEnd, SwingConstants.LEFT);
		this.log.setPreferredSize(new Dimension(500,2000));
		this.log.setVerticalAlignment(SwingConstants.TOP);
		
		this.logScroll = new JScrollPane(this.log);
		this.logScroll.setPreferredSize(new Dimension(500,650));
		this.logScroll.getVerticalScrollBar().setUnitIncrement(15);
		
		this.title = new JLabel("Log");
		
		clear = new JButton("Clear Log");
		clear.addActionListener(this);
		
		this.add(title);
		this.add(logScroll);
		this.add(clear);
	}

	public void error(String message)
	{
		this.append("ERROR", message);
	}

	public void debug(String message)
	{
		this.append("DEBUG", message);
	}
	
	public void info(String message)
	{
		this.append("INFO", message);
	}
	
	private void append(String type, String message)
	{
		String timeStamp = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]").format(new Date());
		String color = "black";
		if(type.equals("ERROR"))
		{
			color = "red";
		}
		else if(type.equals("DEBUG"))
		{
			color = "green";
		}
		this.logList = "<div style='color: "+color+"'>"+timeStamp+" "+type+": "+message+"</div>" + this.logList;
		this.log.setText(this.logInit+this.logList+this.logEnd);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == clear)
		{
			this.log.setText("");
			this.logList = "";
		}
		
	}
}
