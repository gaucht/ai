package com.tgauch.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import com.tgauch.maze.Maze;
import com.tgauch.maze.MazePanel;
import com.tgauch.maze.PathFinding;
import com.tgauch.maze.Tile;


public class Main extends JFrame implements ActionListener {

	private Maze currentMaze;
	private MazePanel mazePanel;
	private JButton generate;
	private JTextField size, roomsize;
	private JPanel input, mazes;
	private LogPanel logKey;
	private JLabel sLabel, rsLabel;
	private int frameCount = 0, fps;
	private boolean running = false;
	public PathFinding p;
	
	public Main()
	{
		this.setSize(1000,1000);
		this.input = new JPanel();
		this.mazes = new JPanel();
		this.logKey = new LogPanel();
		
		this.generate = new JButton("Generate Maze");
		
		this.generate.addActionListener(this);
		
		
		this.size = new JTextField(3);
		this.size.setText("64");
		
		
		this.roomsize = new JTextField(3);
		this.roomsize.setText("10");
		
		this.sLabel = new JLabel("Size: ");
		this.rsLabel = new JLabel("Tile Size: ");
		

		this.input.add(this.sLabel);
		this.input.add(this.size);
		this.input.add(this.rsLabel);
		this.input.add(this.roomsize);
		this.input.add(this.generate);
		
		this.add(this.input, BorderLayout.NORTH);
		this.currentMaze = new Maze(64);
		this.currentMaze.generateRandomMaze();
		this.mazePanel = new MazePanel(this.currentMaze, "Maze");
		p = new PathFinding(this.currentMaze);
		mazePanel.setPathFinder(p);
		this.mazePanel.setTileSize(10);
		p.findPath();
		p.showPath();
		
		
		
		this.mazes.add(this.mazePanel, BorderLayout.EAST);
		
		this.logKey.setPreferredSize(new Dimension(500,725));
		this.mazes.add(logKey, BorderLayout.WEST);
		this.add(this.mazes);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	public void runRefreshLoop()
	   {
	      Thread loop = new Thread()
	      {
	         public void run()
	         {
	        	 running = true;
	            refreshLoop();
	         }
	      };
	      loop.start();
	   }
	
	public void refreshLoop()
	{
		//This value would probably be stored elsewhere.
	      final double GAME_HERTZ = 30.0;
	      //Calculate how many ns each frame should take for our target game hertz.
	      final double TIME_BETWEEN_UPDATES = 1000000000 / GAME_HERTZ;
	      //At the very most we will update the game this many times before a new render.
	      //If you're worried about visual hitches more than perfect timing, set this to 1.
	      final int MAX_UPDATES_BEFORE_RENDER = 5;
	      //We will need the last update time.
	      double lastUpdateTime = System.nanoTime();
	      //Store the last time we rendered.
	      double lastRenderTime = System.nanoTime();
	      
	      //If we are able to get as high as this FPS, don't render again.
	      final double TARGET_FPS = 60;
	      final double TARGET_TIME_BETWEEN_RENDERS = 1000000000 / TARGET_FPS;
	      
	      //Simple way of finding FPS.
	      int lastSecondTime = (int) (lastUpdateTime / 1000000000);
	      
	      while (running)
	      {
	         double now = System.nanoTime();
	         int updateCount = 0;
	         
	            //Render. To do so, we need to calculate interpolation for a smooth render.
	            float interpolation = Math.min(1.0f, (float) ((now - lastUpdateTime) / TIME_BETWEEN_UPDATES) );
	            this.drawMaze(interpolation);
	            lastRenderTime = now;
	         
	            //Update the frames we got.
	            int thisSecond = (int) (lastUpdateTime / 1000000000);
	            if (thisSecond > lastSecondTime)
	            {
	               System.out.println("NEW SECOND " + thisSecond + " " + frameCount);
	               fps = frameCount;
	               frameCount = 0;
	               lastSecondTime = thisSecond;
	            }
	         
	            //Yield until it has been at least the target time between renders. This saves the CPU from hogging.
	            while ( now - lastRenderTime < TARGET_TIME_BETWEEN_RENDERS && now - lastUpdateTime < TIME_BETWEEN_UPDATES)
	            {
	               Thread.yield();
	            
	               //This stops the app from consuming all your CPU. It makes this slightly less accurate, but is worth it.
	               //You can remove this line and it will still work (better), your CPU just climbs on certain OSes.
	               //FYI on some OS's this can cause pretty bad stuttering. Scroll down and have a look at different peoples' solutions to this.
	               try {Thread.sleep(1);} catch(Exception e) {} 
	            
	               now = System.nanoTime();
	            }
	         }
	      }

	public void drawMaze(float interpolation)
	{
		mazePanel.repaint();
		this.repaint();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == generate)
		{
			int m = Integer.parseInt(size.getText());
			int rs = Integer.parseInt(roomsize.getText());
			if(m*rs <= 900)
			{
				this.currentMaze = new Maze(m);
				this.currentMaze.generateRandomMaze();
				this.p.setMaze(this.currentMaze);
				this.findPath();
				this.mazePanel.setTileSize(rs);
				this.mazePanel.setMaze(this.currentMaze);
			}
			else
			{
				this.logKey.error("Size of Maze is too big.");
			}
		}
	}

	private void findPath()
	{
		this.currentMaze.clear();
		p.setHasPath(false);
		p.findPath();
		p.showPath();
		this.logKey.info("Path found");
	}
	
	 public static void main(String[] args)
	   {
	      Main main = new Main();
	      main.setExtendedState(Frame.MAXIMIZED_BOTH);
	      main.setVisible(true);
	      main.runRefreshLoop();
	   }
	
}
