package com.tgauch.maze;

import java.util.ArrayList;
import java.util.Random;


public class Maze{
	private Tile[][] tiles;
	private Random rand;
	private int height;
	private int width;
	private Tile start, end;
	private int tileNumber = 0;
	
	public Maze(int dimension)
	{
		this.height = dimension;
		this.width = dimension;
		tiles = new Tile[height][width];
	} 

	public void generateRandomMaze() {
	
		generateInitialTiles();
		rand = new Random();
		//outer edge is wall so start at 1,1
		int x = 1;
		int y = 1;
		ArrayList<Integer> moves = new ArrayList<Integer>();
		ArrayList<Character> possDirections = new ArrayList<Character>();
		moves.add(y + (x*width));
		this.setStart(this.tiles[1][1]);
		int distance = 2;
		int percent = 100;
		while(moves.size() > 0)
		{
			boolean good = false;
			possDirections.clear();
			if(x == 65)
			{
				int debug;
				debug = 1;
			}
			if(y+distance < this.height-1 && y+distance > 1 && this.tiles[y+distance][x].state == 0)
			{
				good = true;
				possDirections.add('s'); //south
			}
			if(y-distance >= 1 && y-distance < this.height-1 && this.tiles[y-distance][x].state == 0)
			{
				good = true;
				possDirections.add('n'); //north
			}
			if(x+distance >=1 && x+distance < this.width-1 && this.tiles[y][x+distance].state == 0)
			{
				good = true;
				possDirections.add('w'); //west
			}
			if(x-distance >=1 && x-distance < this.width-1 && this.tiles[y][x-distance].state == 0)
			{
				good = true;
				possDirections.add('e'); //east
			}
			
			if(good)
			{
				int move = rand.nextInt(possDirections.size());
				switch(possDirections.get(move))
				{
					case 'n':
						this.tiles[y-distance][x].state = 1;
						this.tiles[y-(distance-1)][x].state = 1;
						y -= distance;
						break;
					case 's':
						this.tiles[y+distance][x].state = 1;
						this.tiles[y+(distance-1)][x].state = 1;
						y += distance;
						break;
					case 'e':
						this.tiles[y][x-(distance)].state = 1;
						this.tiles[y][x-(distance-1)].state = 1;
						x-= distance;
						break;
					case 'w':
						this.tiles[y][x+distance].state = 1;
						this.tiles[y][x+(distance-1)].state = 1;
						x+= distance;
						break;
				}
				
				moves.add(y + (x*width));
			}
			else
			{
				int back = moves.get(moves.size()-1);
				moves.remove(moves.size()-1);
				x = back/width;
				y = back%width;
			}
		}
		this.setEnd(this.tiles[this.height-3][this.width-3]);
	}
	
	private void generateInitialTiles()
	{
		for(int i = 0; i < this.height; i++)
		{
			for(int j = 0; j < this.width; j++)
			{
				this.tiles[i][j] = new Tile(i, j);
				
				if(i == 0)
				{
					this.tiles[i][j].north = null;
				}
				else
				{
					this.tiles[i][j].north = this.tiles[i-1][j];
					this.tiles[i-1][j].south = this.tiles[i][j];
				}
				if(i == this.height - 1)
				{
					this.tiles[i][j].south = null;
				}
				if(j == 0)
				{
					this.tiles[i][j].west = null;
				}
				else
				{
					this.tiles[i][j].west = this.tiles[i][j -1];
					this.tiles[i][j-1].east = this.tiles[i][j];
				}
				if(j == this.width - 1)
				{
					this.tiles[i][j].east = null;
				}
				this.tiles[i][j].state = 0;
				this.tiles[i][j].tileName = this.tileNumber;
				tileNumber++;
				
			}
		}
	}
	public void setVisited(int x, int y)
	{
		this.tiles[x][y].visited = 1;
	}
	public void setPath(int x, int y)
	{
		this.tiles[x][y].visited = 2;
	}
	public int getHeight()
	{
		return this.height;
	}
	public int getWidth()
	{
		return this.width;
	}
	public Tile[][] getTiles()
	{
		return this.tiles;
	}
	public void setTiles(Tile[][] tiles)
	{
		this.tiles = tiles;
	}
	public void setHeight(int height)
	{
		this.height = height;
	}
	public void setWidth(int width)
	{
		this.width = width;
	}
	public void setStart(Tile start)
	{
		if(this.start != null)
		{
			this.start.state = 1;
		}
		if(start != null)
		{
			start.state = 2;
		}
		this.start = start;
	}
	public void setEnd(Tile end)
	{
		if(this.end != null)
		{
			this.end.state = 1;
		}
		if(end != null)
		{
			end.state = 3;
		}
		this.end = end;
	}
	public void setWall(Tile wall, boolean isWall)
	{
		if(this.start.equals(wall))
		{
			this.start = null;
		}
		else if(this.end.equals(wall))
		{
			this.end = null;
		}
		if(isWall)
		{
			wall.state = 0;
		}
		else
		{
			wall.state = 1;
		}
	}
	public Tile getStart()
	{
		return this.start;
	}
	public Tile getEnd()
	{
		return this.end;
	}
	public void clear()
	{
		for(Tile[] t : this.tiles)
		{
			for(Tile t1 : t)
			{
				if(t1.state == 4 || t1.state == 5)
				{
					t1.state = 1;
				}
				t1.prev = null;
			}
		}
	}
}
