package com.tgauch.maze;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import com.tgauch.main.LogPanel;
import com.tgauch.main.Main;

public class MazePanel extends JPanel implements ActionListener,  MouseListener {
	private int sizeBase = 7;
	private int x_cord = 40;
	private int y_cord = 40;
	private int tileSize;
	private JCheckBox showNumbers, showVisitedTiles;
	private JButton clear, blank;
	private JLabel cLabel, vLabel, tLabel;
	private JPanel input, title;
	private Maze maze;
	private PathFinding p;
	public MazePanel(Maze maze, String title)
	{
		this.title = new JPanel();
		this.title.setSize(new Dimension(2000, 2000));
		tLabel = new JLabel("Title:"+title);
		
		this.title.add(tLabel);
		this.add(this.title);
		
		input = new JPanel();
		input.setSize(new Dimension(200,200));
		
		showNumbers = new JCheckBox();
		cLabel = new JLabel("Show Numbers: ");
		showVisitedTiles = new JCheckBox();
		vLabel = new JLabel("Show All Visited Tiles: ");
		clear = new JButton("Clear Maze");
		clear.addActionListener(this);
		blank = new JButton("Blank Maze");
		blank.addActionListener(this);
		
		this.maze = maze;
		this.input.add(cLabel);
		this.input.add(showNumbers);
		this.input.add(vLabel);
		this.input.add(showVisitedTiles);
		this.input.add(clear);
		this.input.add(blank);
		this.add(input);
		this.addMouseListener(this);
		setPreferredSize(new Dimension(1000,725));
	}
	
	public void setPathFinder(PathFinding p)
	{
		this.p = p;
	}
	
	public void setMaze(Maze maze)
	{
		this.maze = maze;
	}
	
	public void setTileSize(int tileSize)
	{
		this.sizeBase = tileSize;
	}
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);

        
        int width = maze.getWidth();
        int height = maze.getHeight();
        Tile[][] tiles = maze.getTiles();
        // could have taken height as well as width
        // just need something to base the tilesize
        tileSize = (width - x_cord) / width + this.sizeBase;
        
        // temp variables used for painting
        int x = x_cord;
        int y = y_cord;

        for (int i = 0; i <= height - 1; i++) {
            for (int j = 0; j <= width - 1; j++) {
            	
	            	//draw tile color
            		
            	//debug();
                if(tiles[i][j].state == 1)
                {
                	g.setColor(Color.WHITE);
                	g.fillRect(x, y, tileSize, tileSize);
                }
                else if(tiles[i][j].state == 0)
                {
                	g.setColor(Color.BLACK);
                	g.fillRect(x, y, tileSize, tileSize);
                }
                else if(tiles[i][j].state == 3)
                {
                	g.setColor(Color.RED);
                	g.fillRect(x, y, tileSize, tileSize);
                }
                else if(tiles[i][j].state == 2)
                {
                	g.setColor(Color.BLUE);
                	g.fillRect(x, y, tileSize, tileSize);
                }
                else if(tiles[i][j].state == 5)
                {
                	if(this.showVisitedTiles.isSelected())
                	{
                		g.setColor(Color.ORANGE);
                		g.fillRect(x, y, tileSize, tileSize);
                		if(this.showNumbers.isSelected())
                		{
                			g.setColor(Color.BLACK);
                			g.setFont(new Font("TimesRoman", Font.PLAIN, tileSize/2));
                			g.drawString(""+tiles[i][j].vistNumber, x+(tileSize/2), y+(tileSize/2));
                		}
                	}
                	else
                	{
                		g.setColor(Color.WHITE);
                		g.fillRect(x, y, tileSize, tileSize);
                	}
                }
                else if (tiles[i][j].state == 4)
                {
                	g.setColor(Color.GREEN);
                	g.fillRect(x, y, tileSize, tileSize);
                	if(this.showNumbers.isSelected())
                	{
                		g.setColor(Color.BLACK);
                		g.setFont(new Font("TimesRoman", Font.PLAIN, tileSize/2));
                		g.drawString(""+tiles[i][j].vistNumber, x+(tileSize/2), y+(tileSize/2));
                	}
                }
                x += tileSize;// change the horizontal
            }// end of inner for loop
            x = x_cord;
            y += tileSize;
        }// end of outer for loop
        
   }

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == clear)
		{
			this.maze.clear();
		}
		else if(e.getSource() == blank)
		{
			this.maze.clear();
			for(int y = 0; y < this.maze.getTiles().length; y++)
			{
				for(int x = 0; x < this.maze.getTiles()[0].length; x++)
				{
					if(x == 0 || y == 0 || x == this.maze.getTiles()[0].length - 1 || y == this.maze.getTiles().length - 1)
					{
						this.maze.getTiles()[y][x].state = 0;
					}
					else
					{
						this.maze.getTiles()[y][x].state = 1;
					}
				}
			}
			this.maze.setEnd(null);
			this.maze.setStart(null);
			p.setMaze(this.maze);
			p.setHasPath(false);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int x = e.getX()-x_cord;
		int y = e.getY()-y_cord;
		boolean changed = false;
		//findTile
		int xTile = (x / tileSize);
		int yTile = (y / tileSize);
		if(xTile > this.maze.getWidth()-1 || yTile > this.maze.getHeight()-1)
		{
			System.out.println("error");
		}
		else
		{
			if(e.getButton() == MouseEvent.BUTTON1)
			{
				if(e.isControlDown())
				{
					this.maze.setWall(this.maze.getTiles()[yTile][xTile], !(this.maze.getTiles()[yTile][xTile].state == 0));
					changed = true;
				}
				else if(!(this.maze.getTiles()[yTile][xTile].state == 3 || this.maze.getTiles()[yTile][xTile].state == 0))
				{
					this.maze.setStart(this.maze.getTiles()[yTile][xTile]);
					changed = true;
				}
			}
			else if(e.getButton() == MouseEvent.BUTTON3 && !(this.maze.getTiles()[yTile][xTile].state == 2 || this.maze.getTiles()[yTile][xTile].state == 0))
			{
				this.maze.setEnd(this.maze.getTiles()[yTile][xTile]);
				changed = true;
			}
		}
		
		if(changed && this.maze.getStart() != null && this.maze.getEnd() != null)
		{
			this.maze.clear();
			this.p.setHasPath(false);
			this.p.findPath();
			this.p.showPath();
			if(this.p.hasNextPoint())
			{
				for(Component c : this.getParent().getComponents())
				{
					if(c.getClass().equals(LogPanel.class))
					{
						LogPanel l = (LogPanel)c;
						l.info("Path Found");
					}
				}
			}
			else
			{
				for(Component c : this.getParent().getComponents())
				{
					if(c.getClass().equals(LogPanel.class))
					{
						LogPanel l = (LogPanel)c;
						l.error("No Path");
					}
				}
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
