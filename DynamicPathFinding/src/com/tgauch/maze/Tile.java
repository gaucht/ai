package com.tgauch.maze;

import java.awt.Point;
import java.util.List;
import java.util.LinkedList;

public class Tile implements Comparable<Tile>{
	public Tile north, south, east, west; //walls of the tile
	public int x, y; //row and column of the maze
	public List<Tile> adj; //list of adjacent tiles
	public int tileName; //name of this current tile
	public Tile prev; //last tile pointer
	public int visited;
	public int g = 0,h = 0,f = 0;
	public int vistNumber;
	public int state = 0; //0 wall 1 walkable 2 start 3 destination
	
	public Tile(int x, int y)
	{
		this.x = x;
		this.y= y;
		this.adj = new LinkedList<Tile>();
		this.prev = null;
		this.tileName = 0;
	}
	
	public int getTileName()
	{	
		return this.tileName;
	}
	
	public boolean equals(Tile b)
	{
		if(this.tileName == b.tileName)
		{
			return true;
		}
		return false;
	}
	
	public List<Tile> getAdj()
	{
		return this.adj;
	}
	
	public Tile getPrev()
	{
		return this.prev;
	}
	
	public int getG()
    {
        return this.g;
    }
    public void setG(int g)
    {
        this.g = g;
    }
    public int getH()
    {
        return this.h;
    }
    public void setH(int h)
    {
        this.h = h;
    }
    public int getF()
    {
        return this.g + this.h;
    }
    public void setPrev(Tile prev)
    {
    	this.prev = prev;
    }
    public Point getLocation()
    {
    	return new Point(x, y);
    }

	@Override
	public int compareTo(Tile o) {
		if(this.equals(o))
		{
			return 0;
		}
		else if(this.getF() > o.getF())
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
}
